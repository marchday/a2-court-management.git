//
//  A2COURTMANAGEMENTAppDelegate.h
//  A2CourtManagement
//
//  Created by liukun on 04/26/2021.
//  Copyright (c) 2021 liukun. All rights reserved.
//

@import UIKit;

@interface A2COURTMANAGEMENTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
