//
//  main.m
//  A2CourtManagement
//
//  Created by liukun on 04/26/2021.
//  Copyright (c) 2021 liukun. All rights reserved.
//

@import UIKit;
#import "A2COURTMANAGEMENTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([A2COURTMANAGEMENTAppDelegate class]));
    }
}
