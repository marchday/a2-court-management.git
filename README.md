# A2CourtManagement

[![CI Status](https://img.shields.io/travis/liukun/A2CourtManagement.svg?style=flat)](https://travis-ci.org/liukun/A2CourtManagement)
[![Version](https://img.shields.io/cocoapods/v/A2CourtManagement.svg?style=flat)](https://cocoapods.org/pods/A2CourtManagement)
[![License](https://img.shields.io/cocoapods/l/A2CourtManagement.svg?style=flat)](https://cocoapods.org/pods/A2CourtManagement)
[![Platform](https://img.shields.io/cocoapods/p/A2CourtManagement.svg?style=flat)](https://cocoapods.org/pods/A2CourtManagement)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

A2CourtManagement is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'A2CourtManagement'
```

## Author

liukun, 184049708@qq.com

## License

A2CourtManagement is available under the MIT license. See the LICENSE file for more info.
