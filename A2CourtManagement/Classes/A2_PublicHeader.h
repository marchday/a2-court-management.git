//
//  A2_PublicHeader.h
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#ifndef A2_PublicHeader_h
#define A2_PublicHeader_h


#import "A2BaseTool.h"
#import "Bubble.h"
#import "DateTools.h"
#import "Masonry.h"
#import "MJRefresh.h"
#import "BRPickerView.h"

#import "A2BaseNav.h"
#import "A2HomeVC.h"
#import "StatusView.h"
#import "A2CourtData.h"
#import "AddView.h"
#import "AddCourtVC.h"
#import "SetMarqueeView.h"
#import "BaseImge.h"

#define kAPPDelegate ((AppDelegate*)[[UIApplication sharedApplication] delegate])
//获取frame
#define getW(CGRect) CGRectGetMaxWidth(CGRect)
#define getH(CGRect) CGRectGetMaxHeight(CGRect)
#define getX(CGRect) CGRectGetMaxX(CGRect)
#define getY(CGRect) CGRectGetMaxY(CGRect)
//弱引用
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self
//获取屏幕宽高
#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHight [UIScreen mainScreen].bounds.size.height
//适配屏幕
#define kScale kScreenWidth/375.0
#define kScaleH  (kScreenHight/667.0)
// 屏幕尺寸
#define ScreenWidth [UIScreen mainScreen].bounds.size.width
#define ScreenHeight [UIScreen mainScreen].bounds.size.height

// 随机颜色
#define RandomColor [UIColor colorWithRed:arc4random_uniform(256)/255.0 green:arc4random_uniform(256)/255.0 blue:arc4random_uniform(256)/255.0 alpha:1]
#define Color(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define ColorAlpha(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
//RGB颜色
#define RGBColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
#define ColorAlpha(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

//白色
#define WColor [UIColor whiteColor]
//黑色
#define BColor [UIColor blackColor]
//红色
#define RColor [UIColor redColor]
//无色
#define CColor [UIColor clearColor]

//文字大小
#define fontSize(s) [UIFont systemFontOfSize:s]
//文字大小粗细
#define fontSizeAndW(s,w) [UIFont systemFontOfSize:s weight:w]
//图片名字
#define imageName(name) [UIImage imageNamed:name]
// 状态栏高度
#define navStatusH [[UIApplication sharedApplication] statusBarFrame].size.height
//导航栏高度
//顶部导航栏高度
#define IS_IPhoneX_All ([UIScreen mainScreen].bounds.size.height == 812 || [UIScreen mainScreen].bounds.size.height == 896)

#define navH  (IS_IPhoneX_All?88:64)
//底部栏高度
#define statusbarHeight_forTools [[UIApplication sharedApplication] statusBarFrame].size.height
#define kTabBarHeight (statusbarHeight_forTools > 20 ? 83 : 49)

#endif /* A2_PublicHeader_h */
