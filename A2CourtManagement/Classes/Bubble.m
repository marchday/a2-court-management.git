

#import "Bubble.h"

@implementation Bubble

//初始化气泡
- (ASPopover *)btnPopover {
  if (!_btnPopover) {
      //初始化气泡
    ASPopoverOption *option = [[ASPopoverOption alloc] init];
      //气泡方向
    option.popoverType = ASPopoverTypeDown;
      //
    option.autoAjustDirection = NO;
      //气泡连接处的宽高
    option.arrowSize = CGSizeMake(0, 0);
      //气泡弹出时,背景颜色
    option.blackOverlayColor = [[UIColor blackColor]colorWithAlphaComponent:0.15];
      //气泡的背景颜色
    option.popoverColor = [UIColor whiteColor];
      //气泡是否收回
    option.dismissOnBlackOverlayTap = YES;
      //气泡出现和收回所用时间
    option.animationIn = 0.5;
    option.animationOut = 0.5;
    //...
    //出来时候弹动效果
    //option.springDamping = 0.5;
    option.initialSpringVelocity = 1;
    option.sideEdge = 7;
    _btnPopover = [[ASPopover alloc] initWithOption:option];
  }
  return _btnPopover;
}


- (ASPopover *)itemPopover {
  if (!_itemPopover) {
      //初始化气泡
    ASPopoverOption *option = [[ASPopoverOption alloc] init];
      //气泡方向
    option.popoverType = ASPopoverTypeUp;
      //
    option.autoAjustDirection = NO;
      //气泡连接处的宽高
    option.arrowSize = CGSizeMake(0, 0);
      //气泡弹出时,背景颜色
      option.blackOverlayColor = [[UIColor blackColor]colorWithAlphaComponent:0.35];
      //气泡的背景颜色
    option.popoverColor = [UIColor whiteColor];
      //气泡是否收回
    option.dismissOnBlackOverlayTap = NO;
      //气泡出现和收回所用时间
    option.animationIn = 0.5;
    option.animationOut = 0.5;
    //...
    //出来时候弹动效果
    //option.springDamping = 0.5;
    option.initialSpringVelocity = 1;
    option.sideEdge = 7;
    
    _itemPopover = [[ASPopover alloc] initWithOption:option];
  }
  return _itemPopover;
}
- (ASPopover *)item2Popover {
  if (!_item2Popover) {
      //初始化气泡
    ASPopoverOption *option = [[ASPopoverOption alloc] init];
      //气泡方向
    option.popoverType = ASPopoverTypeUp;
      //
    option.autoAjustDirection = NO;
      //气泡连接处的宽高
    option.arrowSize = CGSizeMake(9, 6);
      //气泡弹出时,背景颜色
    option.blackOverlayColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
      //气泡的背景颜色
    option.popoverColor = [UIColor whiteColor];
      //气泡是否收回
    option.dismissOnBlackOverlayTap = NO;
      //气泡出现和收回所用时间
    option.animationIn = 0.5;
    option.animationOut = 0.5;
    //...
    //出来时候弹动效果
    //option.springDamping = 0.5;
    option.initialSpringVelocity = 1;
    option.sideEdge = 7;
    
    _item2Popover = [[ASPopover alloc] initWithOption:option];
  }
  return _item2Popover;
}
@end
