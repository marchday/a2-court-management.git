//
//  A2BaseTool.m
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/25.
//

#import "A2BaseTool.h"

@implementation A2BaseTool

+ (void)hideLoadingHUD
{
    [SVProgressHUD dismiss];
}

+ (void)showLoadingHUDWithMessage:(nullable NSString *)message
{
    // 如果当前视图还有其他提示框，就dismiss
    [self hideLoadingHUD];
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setCornerRadius:5];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    
    [SVProgressHUD showWithStatus:message];
    [SVProgressHUD dismissWithDelay:1.5];
}

+ (void)showTextHUDWithMessage:(nullable NSString *)message
{
    [self hideLoadingHUD];
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setCornerRadius:5];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    
    [SVProgressHUD showImage:[UIImage imageNamed:@""] status:message];
    
    [SVProgressHUD dismissWithDelay:1.5];
    
}


+ (void)showCompletionHUDWithMessage:(NSString *)message completion:(completeAction)completion
{
    [self hideLoadingHUD];
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setCornerRadius:5];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    
    [SVProgressHUD showSuccessWithStatus:message];
    
    [SVProgressHUD dismissWithDelay:1.5];
    if (completion) {
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^{
            completion();
        });
    }
    
}
+ (void)showTilieWithMessage:(nullable NSString *)message{
    [self hideLoadingHUD];
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setCornerRadius:5];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    
    [SVProgressHUD showInfoWithStatus:message];
    
    [SVProgressHUD dismissWithDelay:1.5];
}
+ (void)showWarningHUDWithMessage:(NSString *)message completion:(completeAction)completion
{
    [self hideLoadingHUD];
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD setCornerRadius:5];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    
    [SVProgressHUD showErrorWithStatus:message];
    
    [SVProgressHUD dismissWithDelay:1.5];
    if (completion) {
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^{
            completion();
        });
    }
}


+ (void)showLabelText:(NSString *)text {
    UIView *view = [[UIApplication sharedApplication].windows lastObject];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = text;
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:hud afterDelay:1.5];
}

+ (void)showText:(NSString *)text withImageNamed:(NSString *)imageName {
    UIView *view = [[UIApplication sharedApplication].windows lastObject];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    hud.label.text = text;
    UIImage *image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    hud.customView = [[UIImageView alloc] initWithImage:image];
    hud.removeFromSuperViewOnHide = NO;
    [hud hideAnimated:hud afterDelay:1.5];
}

+ (void)showImage:(NSString *)imageName {
    [self showText:@"" withImageNamed:imageName];
}


+ (UILabel *)textColor:(UIColor *)RGB fonSize:(NSInteger)fontSize align:(NSInteger)align{
    UILabel *lable = [[UILabel alloc]init];
    lable.textColor = RGB;
    lable.font = [UIFont systemFontOfSize:fontSize];
    lable.textAlignment = align;
    return lable;
}

+ (UIButton *)image:(NSString *)imageName target:(id)target action:(SEL)action{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return button;
}

+ (UIButton *)title:(NSString *)title titleColor:(UIColor *)titleColor andFont:(float )fontSize target:(id)target action:(SEL)action{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return button;
}

+ (UIImageView *)imageName :(NSString *)imageName imageContentMode:(NSInteger )contentMode{
    UIImageView *image = [[self alloc]init];
    image.image = [UIImage imageNamed:imageName];
    image.contentMode = contentMode;
    return image;
}

+ (UIImageView *)imageData :(NSData *)imageData imageContentMode:(NSInteger )contentMode{
    UIImageView *image = [[self alloc]init];
    image.image = [UIImage imageWithData:imageData];
    image.contentMode = contentMode;
    return image;
}


/**
 * 创建纯色的图片，用来做背景
 */
+ (UIImage *)at_imageWithColor:(UIColor *)color size:(CGSize)size {
    UIGraphicsBeginImageContextWithOptions(size, 0, [UIScreen mainScreen].scale);
    [color set];
    UIRectFill(CGRectMake(0, 0, size.width, size.height));
    UIImage *ColorImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return ColorImg;
}
// 改变图片的大小
+(UIImage *)compressOriginalImage:(UIImage *)image toWidth:(CGFloat)targetWidth
{
    CGSize imageSize = image.size;
    CGFloat Originalwidth = imageSize.width;
    CGFloat Originalheight = imageSize.height;
    CGFloat targetHeight = Originalheight / Originalwidth * targetWidth;
    UIGraphicsBeginImageContext(CGSizeMake(targetWidth, targetHeight));
    [image drawInRect:CGRectMake(0,0,targetWidth,  targetHeight)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

//修改图片大小
+(UIImage*) OriginImage:(UIImage *)image scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);  //size 为CGSize类型，即你所需要的图片尺寸
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //返回已经改变的图片
    return scaledImage;
}

@end
