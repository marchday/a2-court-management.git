//
//  SetMarqueeView.h
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SetMarqueeView : UIView

/**速度*/
@property (nonatomic,assign)CGFloat scrollSpeed;
/**内容*/
@property (nonatomic,copy)NSString *contentStr;


/**保存设置*/
@property (nonatomic,strong)void (^saveSet)(UIColor *textColor,CGFloat scrollSpeed,NSString *contentStr);
/**关闭设置*/
@property (nonatomic,strong)void (^bubbleDissMiss)(void);

@end

NS_ASSUME_NONNULL_END
