//
//  AddCourtVC.h
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddCourtVC : UIViewController

@property (nonatomic,strong)void(^reloadCourtData)(void);

@end

NS_ASSUME_NONNULL_END
