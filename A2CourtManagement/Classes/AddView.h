//
//  AddView.h
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddView : UIView

- (instancetype)initWithFrame:(CGRect)frame andData:(NSDictionary *)dic andIndex:(NSInteger)index andTypeStr:(NSString *)typeStr;

@end

NS_ASSUME_NONNULL_END
