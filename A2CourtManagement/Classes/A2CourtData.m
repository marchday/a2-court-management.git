//
//  A2CourtData.m
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import "A2CourtData.h"

@implementation A2CourtData

/**修改球场状态*/
+ (void)sendChangeMsg :(NSMutableDictionary *)replaceDic andIndex:(NSInteger)index{
    //获取原数组
    NSMutableArray *countDataArr = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]arrayForKey:@"myCourtData"]];
    //替换数据
    [countDataArr replaceObjectAtIndex:index withObject:replaceDic];
    [[NSUserDefaults standardUserDefaults]setObject:countDataArr forKey:@"myCourtData"];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"DataChange" object:nil];
}



+ (NSMutableArray *)a2CountData{
    NSMutableArray *dataArr = [NSMutableArray array];
    NSArray *statusArr = @[@"正常",@"正常",@"异常",@"正常",@"正常"];
    NSArray *timeArr = @[@"12:00-13:00",@"13:00-17:00",@"14:00-16:00",@"19:00-21:00",@"18:00-20:00"];
    NSArray *countArr = @[@"10人",@"6人",@"0人",@"10人",@"10人"];
    NSArray *towelArr = @[@"10",@"10",@"10",@"10",@"10"];
    NSArray *waterArr = @[@"10",@"10",@"10",@"10",@"10"];
    NSArray *manangerArr = @[@"0",@"0",@"0",@"0",@"0"];
    NSArray *signArr = @[@"VIP",@"VIP",@"VIP",@"VIP",@"普通场"];
    for (int i=0; i<statusArr.count; i++) {
        NSDictionary *dic = @{
            @"status":statusArr[i],
            @"count":countArr[i],
            @"time":timeArr[i],
            @"towel":towelArr[i],
            @"water":waterArr[i],
            @"mananger":manangerArr[i],
            @"sign":signArr[i]
        };
        [dataArr addObject:dic];
    }
    
    return dataArr;
}


@end
