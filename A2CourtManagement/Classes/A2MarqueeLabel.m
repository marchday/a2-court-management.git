//
//  A2MarqueeLabel.m
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import "A2MarqueeLabel.h"
#import <Masonry/Masonry.h>

//默认滚动速度
#define kDefaultScrollSpeed 50

@interface A2MarqueeLabel ()<CAAnimationDelegate>
/** 用于显示文字 */
@property (nonatomic, strong) UILabel *label;

@end
@implementation A2MarqueeLabel

#pragma mark -
#pragma mark - View Life Cycle

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupInit];
    }
    return self;
}
- (void)dealloc {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}
#pragma mark -
#pragma mark - Private Method
- (void)setupInit {
    [self addSubview:self.label];
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.mas_equalTo(self);
    }];
    self.clipsToBounds = YES;
    self.scrollSpeed = kDefaultScrollSpeed;
    self.marqueeDirection = SAMarqueeLabelLeft;
    
    [self startAnimation];
}

//播放
- (void)startAnimation {
    
    [self.label.layer removeAnimationForKey:@"animationViewPosition"];
    
    CGPoint pointRightCenter = CGPointMake(self.bounds.size.width + self.label.bounds.size.width/2, self.bounds.size.height/ 2.f);
    CGPoint pointLeftCenter  = CGPointMake(-self.label.bounds.size.width/ 2, self.bounds.size.height / 2.f);
    CGPoint fromPoint        = self.marqueeDirection == SAMarqueeLabelLeft ? pointRightCenter : pointLeftCenter;
    CGPoint toPoint          = self.marqueeDirection == SAMarqueeLabelLeft ? pointLeftCenter  : pointRightCenter;
    
    self.label.center = fromPoint;
    UIBezierPath *movePath    = [UIBezierPath bezierPath];
    [movePath moveToPoint:fromPoint];
    [movePath addLineToPoint:toPoint];
    
    CAKeyframeAnimation *moveAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    moveAnimation.path                 = movePath.CGPath;
    moveAnimation.removedOnCompletion  = YES;
    moveAnimation.duration             = (self.label.bounds.size.width + self.bounds.size.width) / self.scrollSpeed;
    moveAnimation.delegate             = self;
    [self.label.layer addAnimation:moveAnimation forKey:@"animationViewPosition"];
}
//动画播放结束
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if (flag) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startAnimation) object:nil];
        [self startAnimation];
    }
}

#pragma mark-
#pragma mark- Getters && Setters
- (void)setText:(NSString *)text {
    if (![_text isEqualToString:text]) {
        _text = text;
        self.label.text = text;
    }
}
- (void)setFont:(UIFont *)font {
    if (_font != font) {
        _font = font;
        self.label.font = font;
    }
}
- (void)setAttributedText:(NSAttributedString *)attributedText {
    if (![_attributedText.string isEqualToString:attributedText.string]) {
        _attributedText = attributedText;
        self.label.attributedText = attributedText;
    }
}
- (void)setTextColor:(UIColor *)textColor {
    if (_textColor != textColor) {
        _textColor = textColor;
        self.label.textColor = textColor;
    }
    
}
- (void)setShandowColor:(UIColor *)shandowColor {
    if (_shandowColor != shandowColor) {
        _shandowColor = shandowColor;
        self.label.shadowColor = shandowColor;
    }
}
- (void)setShandowOffset:(CGSize)shandowOffset {
    _shandowOffset = shandowOffset;
    self.label.shadowOffset = shandowOffset;
}
- (void)setTextAlignment:(NSTextAlignment)textAlignment {
    _textAlignment = textAlignment;
    self.label.textAlignment = textAlignment;
}
- (void)setMarqueeDirection:(A2MarqueeLabelDirection)marqueeDirection {
    _marqueeDirection = marqueeDirection;
    [self setNeedsLayout];
}
- (UILabel *)label {
    if (!_label) {
        _label = [[UILabel alloc] initWithFrame:self.bounds];
        _label.backgroundColor = [UIColor clearColor];
        _label.textAlignment = NSTextAlignmentLeft;
    }
    return _label;
}
@end
