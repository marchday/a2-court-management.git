//
//  A2CourtCell.m
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import "A2CourtCell.h"
#import "A2_PublicHeader.h"

@implementation A2CourtCell

- (UIButton *)statusBtn{
    if (!_statusBtn) {
        _statusBtn = [A2BaseTool title:@"正常" titleColor:WColor andFont:13 target:self action:@selector(statusBtnClick :)];
        _statusBtn.frame = CGRectMake(10*kScale, 10*kScale, 84*kScale, 22.5*kScale);
        _statusBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_statusBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5*kScale, 0, -5*kScale)];
        [self.contentView addSubview:_statusBtn];
    }
    return _statusBtn;
}
- (void)statusBtnClick:(UIButton *)btn{
    [self.delegate changeCourtStatus:btn];
}
- (UIButton *)timeBtn{
    if (!_timeBtn) {
        _timeBtn = [A2BaseTool title:@"19:00-21:00" titleColor:WColor andFont:13 target:self action:@selector(timeBtnClick :)];
        _timeBtn.frame = CGRectMake(10*kScale, getY(self.statusBtn.frame)+12*kScale, 84*kScale, 22.5*kScale);
        _timeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_timeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5*kScale, 0, -5*kScale)];
        [self.contentView addSubview:_timeBtn];
    }
    return _timeBtn;
}
- (void)timeBtnClick:(UIButton *)btn{
    [self.delegate changeCourtTime:btn];
}

- (UIButton *)countBtn{
    if (!_countBtn) {
        _countBtn = [A2BaseTool title:@"10人" titleColor:WColor andFont:13 target:self action:@selector(countBtnClick :)];
        _countBtn.frame = CGRectMake(10*kScale, getY(self.timeBtn.frame)+12*kScale, 84*kScale, 22.5*kScale);
        _countBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_countBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5*kScale, 0, -5*kScale)];
        [self.contentView addSubview:_countBtn];
    }
    return _countBtn;
}
- (void)countBtnClick:(UIButton *)btn{
    [self.delegate changeCount:btn];
}

- (UIButton *)towelBtn{
    if (!_towelBtn) {
        _towelBtn = [A2BaseTool image:@"mao jin" target:self action:@selector(towelBtnClick :)];
        _towelBtn.frame = CGRectMake(95*kScale, 175*kScale, 25*kScale, 21*kScale);
        [self.contentView addSubview:_towelBtn];
    }
    return _towelBtn;
}

- (void)towelBtnClick :(UIButton *)btn{
    [self.delegate addTowe:btn];
}
- (UIButton *)waterBtn{
    if (!_waterBtn) {
        _waterBtn = [A2BaseTool image:@"shui" target:self action:@selector(waterBtnClick :)];
        _waterBtn.frame = CGRectMake(getX(self.towelBtn.frame)+17.5*kScale, 170*kScale, 15*kScale, 30*kScale);
        [self.contentView addSubview:_waterBtn];
    }
    return _waterBtn;
}

- (void)waterBtnClick :(UIButton *)btn{
    [self.delegate addWater:btn];
}

- (UILabel *)signLable{
    if (!_signLable) {
        _signLable = [A2BaseTool textColor:RGBColor(51, 51, 51) fonSize:14 align:1];
        _signLable.frame = CGRectMake(114*kScale, 0, 50*kScale, 20*kScale);
        [_signLable.layer setCornerRadius:3*kScale];
        [_signLable.layer setMasksToBounds:YES];
        _signLable.font = fontSizeAndW(14, 1);
        _signLable.backgroundColor = RGBColor(255, 235, 0);
        [self.contentView addSubview:_signLable];
    }
    return _signLable;
}


@end
