//
//  BaseImge.h
//  NewBallView
//
//  Created by DPCTESSORCT on 2020/8/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseImge : NSObject

+ (UIImage *)zlc_imageNamed:(NSString *)name ofType:(nullable NSString *)type;

@end

NS_ASSUME_NONNULL_END
