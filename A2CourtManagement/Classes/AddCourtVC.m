//
//  AddCourtVC.m
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import "AddCourtVC.h"

#import "AddCourtVC.h"
#import "A2_PublicHeader.h"

@interface AddCourtVC ()<UITextFieldDelegate>

/**输入数组*/
@property (nonatomic,strong)NSMutableArray *inputArr;
/**选中按钮*/
@property (nonatomic,strong)UIButton *selectBtn;


@end

@implementation AddCourtVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = BColor;
    self.navigationItem.title = @"添加";
    _inputArr = [NSMutableArray array];
    [self creatUI];
}

- (void)creatUI{
    NSArray *titleArr = @[@"球场类型",@"馆场时间",@"管理员"];
    NSArray *placeTextArr = @[@"VIP/普通场",@"1-10小时",@""];
    
    for(int i=0;i<titleArr.count;i++){
        //标题
        UILabel *title = [A2BaseTool textColor:WColor fonSize:14 align:0];
        title.text = titleArr[i];
        title.frame = CGRectMake(15*kScale, 0, 75*kScale, 50*kScale);
        
        //输入
        UITextField *input = [[UITextField alloc]initWithFrame:CGRectMake(15*kScale, navH+(15+i*55)*kScale, 345*kScale, 50*kScale)];
        input.backgroundColor = [WColor colorWithAlphaComponent:0.15];
        [input.layer setCornerRadius:3];
        [input.layer setMasksToBounds:YES];
        input.textAlignment = NSTextAlignmentRight;
        input.textColor = WColor;
        input.font = fontSize(14);
        input.delegate = self;
        input.tag = i;
        input.attributedPlaceholder = [[NSAttributedString alloc]initWithString:placeTextArr[i] attributes:@{
            NSForegroundColorAttributeName:WColor,
            NSFontAttributeName:fontSize(14),
        }];
        input.leftViewMode = UITextFieldViewModeAlways;
        input.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 90*kScale, 50*kScale)];
        [input.leftView addSubview:title];
        input.rightViewMode = UITextFieldViewModeAlways;
        //右侧内容
        if (i<2) {
            input.rightView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 15*kScale, 50*kScale)];
        }else{
            input.rightView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 266*kScale, 50*kScale)];
            for (int c=0; c<3; c++) {
                UIButton *btn = [A2BaseTool title:[NSString stringWithFormat:@"管理%d",c+1] titleColor:WColor andFont:14 target:self action:@selector(selectManangar :)];
                btn.tag = c;
                btn.frame = CGRectMake(c*86.5*kScale, 0, 74.5*kScale, 50*kScale);
                [btn setImage:[BaseImge zlc_imageNamed:@"Sound unchecked@2x" ofType:@".png"] forState:UIControlStateNormal];
                [btn setImage:[BaseImge zlc_imageNamed:@"Sound selected@2x" ofType:@".png"] forState:UIControlStateSelected];
                if (c==0) {
                    btn.selected = YES;
                    _selectBtn = btn;
                }
                [input.rightView addSubview:btn];
            }
            
            
        }
        [_inputArr addObject:input];
        [self.view addSubview:input];
    }
    
    
    //保存
    UIButton *saveBtn = [A2BaseTool title:@"保存" titleColor:WColor andFont:16 target:self action:@selector(saveBtnClick)];
    saveBtn.frame = CGRectMake(45*kScale, 460*kScale, 285*kScale, 55*kScale);
    saveBtn.backgroundColor = RGBColor(259, 91, 11);
    [saveBtn.layer setCornerRadius:5*kScale];
    [saveBtn.layer setMasksToBounds:YES];
    [self.view addSubview:saveBtn];
}

- (void)saveBtnClick{
    /**球场类型*/
    NSString *courtType = ((UITextField *)[_inputArr objectAtIndex:0]).text;
    /**订场时间*/
    NSString *courtTime = ((UITextField *)[_inputArr objectAtIndex:1]).text;
    
    
    if (courtType.length < 1) {
        [A2BaseTool showTilieWithMessage:@"请选择球场类型"];
    }else if(courtTime.length < 1){
        [A2BaseTool showTilieWithMessage:@"请选择订场时间"];
    }else{
        NSInteger dateHour = [courtTime intValue] + [NSDate date].br_hour;
        if (dateHour > 24) {
            dateHour -= 24;
        }
        NSString *toDateStr = [NSString stringWithFormat:@"%ld:%02ld",dateHour,[NSDate date].br_minute];
        
        NSString *timeStr = [NSString stringWithFormat:@"%@-%@",[[NSDate date]formattedDateWithFormat:@"HH:mm"],toDateStr];
        /**管理员*/
        NSString *mananger = _selectBtn.titleLabel.text;
        
        NSDictionary *dic = @{
            @"status":@"正常",
            @"count":@"10人",
            @"time":timeStr,
            @"towel":@"10",
            @"water":@"10",
            @"mananger":mananger,
            @"sign":courtType,
        };
        //获取原数组
        NSMutableArray *countDataArr = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]arrayForKey:@"myCourtData"]];
        [countDataArr addObject:dic];
        [[NSUserDefaults standardUserDefaults]setObject:countDataArr forKey:@"myCourtData"];
        self.reloadCourtData();
        [A2BaseTool showCompletionHUDWithMessage:@"添加成功" completion:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
    
}

#pragma mark-
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField.tag == 0) {
        //场馆类型选择
        [self openPickerView:textField andPickerData:@[@"VIP",@"普通场"]];
        
    }else if(textField.tag == 1){
        //场馆时间选择
        NSMutableArray *arr = [NSMutableArray array];
        for (int i=1; i<11; i++) {
            [arr addObject:[NSString stringWithFormat:@"%d",i]];
        }
        [self openPickerView:textField andPickerData:arr];
    }
    
    
    return NO;
}

- (void)openPickerView :(UITextField *)textField andPickerData:(NSArray *)dataSourceArr{
    BRStringPickerView *pickerView = [[BRStringPickerView alloc]initWithPickerMode:BRStringPickerComponentSingle];
    pickerView.dataSourceArr = dataSourceArr;
    pickerView.resultModelBlock = ^(BRResultModel * _Nullable resultModel) {
        textField.text = resultModel.value;
    };
    [pickerView show];
}

//选择管理
- (void)selectManangar :(UIButton *)btn{
    if (btn == _selectBtn) {
        btn.selected = YES;
    }else{
        btn.selected = YES;
        _selectBtn.selected = NO;
        _selectBtn = btn;
    }
}

@end
