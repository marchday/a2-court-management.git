
#import <Foundation/Foundation.h>
#import <PopoverObjC/ASPopover.h>

NS_ASSUME_NONNULL_BEGIN

@interface Bubble : NSObject

@property (nonatomic, strong) ASPopover *btnPopover;
@property (nonatomic, strong) ASPopover *itemPopover;
@property (nonatomic, strong) ASPopover *item2Popover;

@end

NS_ASSUME_NONNULL_END
