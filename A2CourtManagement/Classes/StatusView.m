//
//  StatusView.m
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import "StatusView.h"
#import "A2_PublicHeader.h"

@interface StatusView ()

/**数据*/
@property (nonatomic,strong)NSDictionary *dic;
/**下标*/
@property (nonatomic,assign)NSInteger index;
/**状态*/
@property (nonatomic,strong)NSString *statusStr;


@end

@implementation StatusView

- (instancetype)initWithFrame:(CGRect)frame andData:(NSDictionary *)dic andIndex:(NSInteger)index{
    self = [super initWithFrame:frame];
    if (self) {
        _dic = dic;
        _index = index;
        [self creatUI];
    }
    return self;
}

- (void)creatUI{
    
    _statusStr = _dic[@"status"];

    
    UIView *view = [[UIView alloc]initWithFrame:self.bounds];
    view.backgroundColor = WColor;
    [view.layer setCornerRadius:3*kScale];
    [view.layer setMasksToBounds:YES];
    [self addSubview:view];
    
    //图标
    UIImageView *iconImg = [[UIImageView alloc]initWithFrame:CGRectMake(110*kScale, 30*kScale, 40*kScale, 45*kScale)];
    [view addSubview:iconImg];
    
    //按钮
    UIButton *changeBtn = [A2BaseTool title:@"" titleColor:WColor andFont:16 target:self action:@selector(changeStatus :)];
    changeBtn.frame = CGRectMake(55*kScale, 115*kScale, 150*kScale, 50*kScale);
    [changeBtn.layer setCornerRadius:3*kScale];
    [changeBtn.layer setMasksToBounds:YES];
    changeBtn.backgroundColor = RGBColor(248, 164, 0);
    [view addSubview:changeBtn];
    
    
    if ([_statusStr isEqual:@"正常"]) {
        iconImg.image = [BaseImge zlc_imageNamed:@"yichang" ofType:@".png"];
        [changeBtn setTitle:@"异常" forState:UIControlStateNormal];
    }else{
        iconImg.image = [BaseImge zlc_imageNamed:@"zhengchang" ofType:@".png"];
        [changeBtn setTitle:@"正常" forState:UIControlStateNormal];
    }
}

- (void)changeStatus :(UIButton *)btn{
    
    //修改数据
    NSMutableDictionary *replaceDic = [NSMutableDictionary dictionaryWithDictionary:_dic];
    replaceDic[@"status"] = btn.titleLabel.text;
    
    //替换数据
    [A2CourtData sendChangeMsg:replaceDic andIndex:_index];
}
@end
