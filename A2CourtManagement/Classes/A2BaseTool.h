//
//  A2BaseTool.h
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/25.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "SVProgressHUD.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^__nullable completeAction)(void);

@interface A2BaseTool : NSObject

/**
 文字弹窗
 @param message     文字+加载框
 */
+ (void)showLoadingHUDWithMessage:(nullable NSString *)message;

//展示警告
+ (void)showTilieWithMessage:(nullable NSString *)message;
/**
 文字弹窗
 @param message     纯文字
 */
+ (void)showTextHUDWithMessage:(nullable NSString *)message;
/**
 文字弹窗
 @param message     完成提示
 @param completion 完成回掉方法
 */
+ (void)showCompletionHUDWithMessage:(nullable NSString *)message completion:(completeAction)completion;
/**
 文字弹窗
 @param message     失败提示
 @param completion 失败回掉方法
 */
+ (void)showWarningHUDWithMessage:(nullable NSString *)message completion:(completeAction)completion;

/**
 文字弹窗
 @param text     文字
 */
+ (void)showLabelText:(NSString *)text;
/**
 文字图片弹窗
 @param text     文字
 @prarm imageName 图片
 */
+ (void)showText:(NSString *)text withImageNamed:(NSString *)imageName;
/**
 图片弹窗
 @prarm imageName 图片
 */
+ (void)showImage:(NSString *)imageName;

/**
 便利构造函数

 @param RGB     文字颜色
 
 @param fontSize    字体大小
 
 @param align    对齐方式
 
 @return UILabel 实例
 */
+ (UILabel *)textColor:(UIColor *)RGB fonSize:(NSInteger)fontSize align:(NSInteger)align;

/**
 便利构造函数

 @param imageName     图片名称
 @param target    代理
 @param action    响应事件
 @return UIButton 实例
 */
+ (UIButton *)image:(NSString *)imageName target:(id)target action:(SEL)action;

/**
 便利构造函数

 @param title     提示问题
 @param target    代理
 @param action    响应事件
 @return UIButton 实例
 */
+ (UIButton *)title:(NSString *)title titleColor:(UIColor *)titleColor andFont:(float )fontSize target:(id)target action:(SEL)action;


/**
 便利构造函数

 @param imageName     图片名字
 
 @param contentMode    图片放置方式
 
 @return UIImage 实例
 */

+ (UIImageView *)imageName :(NSString *)imageName imageContentMode:(NSInteger )contentMode;

/**
 便利构造函数

 @param imageData     图片数据
 
 @param contentMode    图片放置方式
 
 @return UIImage 实例
 */
+ (UIImageView *)imageData :(NSData *)imageData imageContentMode:(NSInteger )contentMode;

/**
 便利构造函数

 @param color  颜色
 
 @param size    大小
 
 @return UIImage 实例
 */
+ (UIImage *)at_imageWithColor:(UIColor *)color size:(CGSize)size;
/**
 便利构造函数

 @param image  图片
 
 @param targetWidth    修改宽度
 
 @return UIImage 新图片
 */
+ (UIImage *)compressOriginalImage:(UIImage *)image toWidth:(CGFloat)targetWidth;
/**
 便利构造函数

 @param image  图片
 
 @param size    裁剪宽高
 
 @return UIImage 新图片
 */
+ (UIImage*) OriginImage:(UIImage *)image scaleToSize:(CGSize)size;

@end

NS_ASSUME_NONNULL_END
