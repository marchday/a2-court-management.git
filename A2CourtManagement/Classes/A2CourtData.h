//
//  A2CourtData.h
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface A2CourtData : NSObject

/**修改球场数据*/
+ (void)sendChangeMsg :(NSMutableDictionary *)replaceDic andIndex:(NSInteger)index;
/**获取球场数据*/
+ (NSMutableArray *)a2CountData;

@end

NS_ASSUME_NONNULL_END
