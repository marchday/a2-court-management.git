//
//  A2HomeVC.m
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import "A2HomeVC.h"
#import "A2_PublicHeader.h"
#import "A2MarqueeLabel.h"
#import "A2CourtCell.h"
#import "AddCourtVC.h"

static NSString *cellID = @"cell";

@interface A2HomeVC ()<UICollectionViewDelegate,UICollectionViewDataSource,A2CourtCellDelegate>

/**顶部跑马灯*/
@property (nonatomic,strong)A2MarqueeLabel *marqueelable;
/**列表数据*/
@property (nonatomic,strong)NSMutableArray *countDataArr;
/**列表视图*/
@property (nonatomic,strong)UICollectionView *courtListView;
/**气泡*/
@property (nonatomic,strong)Bubble *bubble;

@end


@implementation A2HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = BColor;
    self.navigationItem.title = @"篮球场管理";
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    self.navigationItem.backBarButtonItem.tintColor = WColor;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[BaseImge zlc_imageNamed:@"jia@2x" ofType:@".png"] style:UIBarButtonItemStyleDone target:self action:@selector(setMarqueeText)];
    self.navigationItem.rightBarButtonItem.tintColor = RGBColor(255, 171, 0);
    
    _bubble= [Bubble new];
    
    //修改通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadCourtDataArr) name:@"DataChange" object:nil];
    
    [self creatUI];
}
- (void)viewWillAppear:(BOOL)animated{
    [self.marqueelable startAnimation];
}




- (void)reloadCourtDataArr{
    _countDataArr = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults]arrayForKey:@"myCourtData"]];
    if (_countDataArr.count == 0) {
        [_countDataArr addObjectsFromArray:[A2CourtData a2CountData]];
        [[NSUserDefaults standardUserDefaults]setObject:_countDataArr forKey:@"myCourtData"];
    }
    
    [self.courtListView reloadData];
    [self.courtListView.mj_header endRefreshing];
    [self.bubble.btnPopover dismiss];
}


- (void)creatUI{
    /**头部滚动条*/
    [self.view addSubview:self.marqueelable];
    /**列表视图*/
    [self.view addSubview:self.courtListView];
    /**添加球场*/
    UIButton *addBtn = [A2BaseTool image:@"jia" target:self action:@selector(addCourt)];
    [addBtn setImage:[BaseImge zlc_imageNamed:@"jia@2x" ofType:@".png"] forState:UIControlStateNormal];
    addBtn.frame = CGRectMake(295*kScale, kScreenHight-50*kScale-40*kScaleH-kTabBarHeight, 50*kScale, 50*kScale);
    [self.view addSubview:addBtn];
    
}
/**添加球场*/
- (void)addCourt{
    self.hidesBottomBarWhenPushed = YES;
    AddCourtVC *addCourt = [AddCourtVC new];
    addCourt.reloadCourtData = ^{
        [self reloadCourtDataArr];
    };
    [self.navigationController pushViewController:addCourt animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

/**列表视图*/
- (UICollectionView *)courtListView{
    if (!_courtListView) {
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.sectionInset = UIEdgeInsetsMake(12.5*kScale, 15*kScale, 10*kScale, 15*kScale);
        flowLayout.itemSize = CGSizeMake(165*kScale, 210*kScale);
        flowLayout.minimumLineSpacing = 10*kScale;
        flowLayout.minimumInteritemSpacing = 15*kScale;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        _courtListView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, getY(self.marqueelable.frame), kScreenWidth, kScreenHight-kTabBarHeight-getY(self.marqueelable.frame)) collectionViewLayout:flowLayout];
        _courtListView.delegate = self;
        _courtListView.dataSource = self;
        [_courtListView registerClass:[A2CourtCell class] forCellWithReuseIdentifier:cellID];
        _courtListView.mj_header = [MJRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(reloadCourtDataArr)];
        [_courtListView.mj_header beginRefreshing];
        [self.view addSubview:_courtListView];
        
    }
    return _courtListView;
}

#pragma mark-
#pragma mark-UICollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _countDataArr.count;
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    A2CourtCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    
    cell.contentView.backgroundColor = [WColor colorWithAlphaComponent:0.15];
    [cell.contentView.layer setCornerRadius:3*kScale];
    cell.statusBtn.tag = indexPath.item;
    cell.countBtn.tag = indexPath.item;
    cell.timeBtn.tag = indexPath.item;
    cell.waterBtn.tag = indexPath.item;
    cell.towelBtn.tag = indexPath.item;
    cell.delegate = self;
    
    //标记
    cell.signLable.text = _countDataArr[indexPath.item][@"sign"];
    //状态
    [cell.statusBtn setTitle:_countDataArr[indexPath.item][@"status"] forState:UIControlStateNormal];
    //时间
    [cell.timeBtn setTitle:_countDataArr[indexPath.item][@"time"] forState:UIControlStateNormal];
    
    //人数
    if ([cell.statusBtn.titleLabel.text isEqual:@"正常"]) {
        [cell.countBtn setTitle:_countDataArr[indexPath.item][@"count"] forState:UIControlStateNormal];
        [cell.waterBtn setImage:[BaseImge zlc_imageNamed:@"shui@2x" ofType:@".png"] forState:UIControlStateNormal];
        [cell.towelBtn setImage:[BaseImge zlc_imageNamed:@"mao jin@2x" ofType:@".png"] forState:UIControlStateNormal];
        [cell.statusBtn setImage:[BaseImge zlc_imageNamed:@"zhengchang@2x" ofType:@".png"] forState:UIControlStateNormal];
    }else{
        [cell.countBtn setTitle:@"0人" forState:UIControlStateNormal];
        [cell.waterBtn setImage:[BaseImge zlc_imageNamed:@"s@2x" ofType:@".png"] forState:UIControlStateNormal];
        [cell.towelBtn setImage:[BaseImge zlc_imageNamed:@"mj@2x" ofType:@".png"] forState:UIControlStateNormal];
        [cell.statusBtn setImage:[BaseImge zlc_imageNamed:@"yichang@2x" ofType:@".png"] forState:UIControlStateNormal];
    }
    
    
    
    [cell.countBtn setImage:[BaseImge zlc_imageNamed:@"renshu@2x" ofType:@".png"] forState:UIControlStateNormal];
    [cell.timeBtn setImage:[BaseImge zlc_imageNamed:@"shijian@2x" ofType:@".png"] forState:UIControlStateNormal];
    
    return cell;
}

#pragma mark-cellDelegate
- (void)changeCourtStatus:(UIButton *)btn{
    
    StatusView *statusView = [[StatusView alloc]initWithFrame:CGRectMake(0, 0, 260*kScale, 180*kScale) andData:_countDataArr[btn.tag] andIndex:btn.tag];
    
    [self.bubble.btnPopover show:statusView atPoint:CGPointMake(self.view.center.x, 243.5*kScaleH)];
}
- (void)changeCourtTime:(UIButton *)btn{
    
}
- (void)changeCount:(UIButton *)btn{
    
}
/**加毛巾*/
- (void)addTowe:(UIButton *)btn{
    if ([self getCourtStatus:btn.tag]) {
        AddView *addView = [[AddView alloc]initWithFrame:CGRectMake(0, 0, 260*kScale, 180*kScale) andData:_countDataArr[btn.tag] andIndex:btn.tag andTypeStr:@"加毛"];
        
        [self.bubble.btnPopover show:addView atPoint:CGPointMake(self.view.center.x, 243.5*kScaleH)];
    }
}

/**加水*/
- (void)addWater:(UIButton *)btn{
    if ([self getCourtStatus:btn.tag]) {
        AddView *addView = [[AddView alloc]initWithFrame:CGRectMake(0, 0, 260*kScale, 180*kScale) andData:_countDataArr[btn.tag] andIndex:btn.tag andTypeStr:@"加水"];
        
        [self.bubble.btnPopover show:addView atPoint:CGPointMake(self.view.center.x, 243.5*kScaleH)];
    }
}

//返回当前状态
- (BOOL)getCourtStatus :(NSInteger)index{
    NSDictionary *dic = _countDataArr[index];
    if ([dic[@"status"] isEqual:@"异常"]) {
        
        [A2BaseTool showTilieWithMessage:@"此球场异常 请先修改球场状态"];
        
        return NO;
    }
    return YES;
}




/**头部滚动条*/
- (A2MarqueeLabel *)marqueelable{
    if (!_marqueelable) {
        _marqueelable = [[A2MarqueeLabel alloc]initWithFrame:CGRectMake(0, navH, kScreenWidth, 44*kScale)];
        _marqueelable.text = @"一号场需要五瓶水 二号场需要两个水 五号场异常";
        _marqueelable.textColor = WColor;
        _marqueelable.backgroundColor = RGBColor(211, 0, 0);
        _marqueelable.font = fontSize(14);
        _marqueelable.scrollSpeed = 50;
    }
    return _marqueelable;
}

//设置跑马灯
- (void)setMarqueeText{
    __block Bubble *bub = [Bubble new];
    bub.btnPopover.option.popoverColor = CColor;
    
    
    //设置跑马灯
    SetMarqueeView *setMarqueeView = [[SetMarqueeView alloc]initWithFrame:self.view.bounds];
    setMarqueeView.backgroundColor = CColor;
    setMarqueeView.contentStr = @"一号场需要五瓶水 二号场需要两个水 五号场异常";
    setMarqueeView.scrollSpeed = 50;
    setMarqueeView.bubbleDissMiss = ^{
        [bub.btnPopover dismiss];
    };
    
    
    WS(weakSelf);
    setMarqueeView.saveSet = ^(UIColor * _Nonnull textColor, CGFloat scrollSpeed, NSString * _Nonnull contentStr){
        weakSelf.marqueelable.text = contentStr;
        weakSelf.marqueelable.scrollSpeed = scrollSpeed;
        weakSelf.marqueelable.textColor = textColor;
        [bub.btnPopover dismiss];
    };
    [bub.btnPopover show:setMarqueeView atPoint:CGPointMake(0, 0)];
}

@end
