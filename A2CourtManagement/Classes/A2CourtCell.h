//
//  A2CourtCell.h
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol A2CourtCellDelegate <NSObject>

//切换状态
- (void)changeCourtStatus :(UIButton *)btn;
//更换时间
- (void)changeCourtTime :(UIButton *)btn;
//修改人数
- (void)changeCount :(UIButton *)btn;
//加毛巾
- (void)addTowe :(UIButton *)btn;
//加水
- (void)addWater :(UIButton *)btn;

@end

@interface A2CourtCell : UICollectionViewCell

/**状态*/
@property (nonatomic,strong)UIButton *statusBtn;
/**时间*/
@property (nonatomic,strong)UIButton *timeBtn;
/**人数*/
@property (nonatomic,strong)UIButton *countBtn;
/**毛巾*/
@property (nonatomic,strong)UIButton *towelBtn;
/**水瓶*/
@property (nonatomic,strong)UIButton *waterBtn;
/**标记*/
@property (nonatomic,strong)UILabel *signLable;
/**代理*/
@property (nonatomic,weak)id<A2CourtCellDelegate>delegate;

@end


NS_ASSUME_NONNULL_END
