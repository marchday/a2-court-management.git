//
//  A2BaseNav.m
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import "A2BaseNav.h"
#import "A2_PublicHeader.h"

@interface A2BaseNav ()

@end

@implementation A2BaseNav

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //导航背景
    [self.navigationBar setBackgroundImage:[A2BaseTool at_imageWithColor:CColor size:CGSizeMake(kScreenWidth, navH)] forBarMetrics:UIBarMetricsDefault];
    //导航分割线
    self.navigationBar.shadowImage = [UIImage new];
    //状态栏白色
    self.navigationBar.barStyle = UIStatusBarStyleLightContent;
    //设置导航文字
    [self.navigationBar setTitleTextAttributes:@{
        NSFontAttributeName :fontSizeAndW(17, 0.3),
        NSForegroundColorAttributeName :WColor,
    }];
    self.navigationBar.translucent = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
