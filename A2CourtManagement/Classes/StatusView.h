//
//  StatusView.h
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StatusView : UIView

- (instancetype)initWithFrame:(CGRect)frame andData:(NSDictionary *)dic andIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
