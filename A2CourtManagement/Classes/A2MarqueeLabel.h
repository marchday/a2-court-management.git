//
//  A2MarqueeLabel.h
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, A2MarqueeLabelDirection) {
    SAMarqueeLabelLeft,
    SAMarqueeLabelRight
};
/**
 animation 实现跑马灯样式 label
 */
@interface A2MarqueeLabel : UIView
/** 文字 */
@property (nonatomic, copy, nullable) NSString *text;
/** 富文本 */
@property (nonatomic, copy, nullable) NSAttributedString *attributedText;
/** 文字颜色*/
@property (nonatomic, strong, nonnull) UIColor *textColor;
/** 文字font */
@property (nonatomic, strong, nonnull) UIFont *font;
/** 文字阴影颜色 */
@property (nonatomic, strong, nullable) UIColor *shandowColor;
/** 文字阴影偏移 */
@property (nonatomic, assign) CGSize shandowOffset;
/** 文字位置，只在文字不滚动时有效 */
@property (nonatomic, assign) NSTextAlignment textAlignment;
/** 滚动方向，默认 SAMarqueeLabelLeft */
@property (nonatomic, assign) A2MarqueeLabelDirection marqueeDirection;
/** 滚动速度，默认30 */
@property (nonatomic, assign) CGFloat scrollSpeed;

- (void)startAnimation;

@end

NS_ASSUME_NONNULL_END
