//
//  AddView.m
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import "AddView.h"
#import "A2_PublicHeader.h"

@interface AddView ()

/**数据*/
@property (nonatomic,strong)NSDictionary *dic;
/**下标*/
@property (nonatomic,assign)NSInteger index;
/**类型*/
@property (nonatomic,strong)NSString *typeStr;
/**状态*/
@property (nonatomic,strong)UILabel *showCount;
/**数量*/
@property (nonatomic,assign)NSInteger count;

@end

@implementation AddView

- (instancetype)initWithFrame:(CGRect)frame andData:(NSDictionary *)dic andIndex:(NSInteger)index andTypeStr:(NSString *)typeStr{
    self = [super initWithFrame:frame];
    if (self) {
        _typeStr = typeStr;
        _dic = dic;
        _index = index;
        [self creatUI];
    }
    return self;
}

- (void)creatUI{
    
    UIView *view = [[UIView alloc]initWithFrame:self.bounds];
    view.backgroundColor = WColor;
    [view.layer setCornerRadius:3*kScale];
    [view.layer setMasksToBounds:YES];
    [self addSubview:view];
    
    //图标
    UIImageView *iconImg = [[UIImageView alloc]init];
    if ([_typeStr isEqual: @"加水"]) {
        iconImg.image = [BaseImge zlc_imageNamed:@"s@2x" ofType:@".png"];
        iconImg.frame = CGRectMake(122.5*kScale, 15*kScale, 15*kScale, 30*kScale);
    }else{
        iconImg.image = [BaseImge zlc_imageNamed:@"mj@2x" ofType:@".png"];
        iconImg.frame = CGRectMake(122.5*kScale, 15*kScale, 18*kScale, 21*kScale);
    }
    
    [view addSubview:iconImg];
    
    //数量显示
    UILabel *showWaterCount = [A2BaseTool textColor:RGBColor(51, 51, 51) fonSize:30 align:1];
    showWaterCount.frame = CGRectMake(106*kScale, 59*kScale, 48*kScale, 42*kScale);
    
    
    if ([_typeStr isEqual: @"加水"]) {
        _count =  [_dic[@"water"] intValue];
        showWaterCount.text =  _dic[@"water"];
    }else{
        _count =  [_dic[@"towel"] intValue];
        showWaterCount.text =  _dic[@"towel"];
    }
    
    
    _showCount = showWaterCount;
    [view addSubview:showWaterCount];
    
    //数量调整
    NSArray *iconArr = @[@"jian",@"jia"];
    for (int i=0; i<2; i++) {
        UIButton *btn = [A2BaseTool image:@"" target:self action:@selector(changeWaterCount :)];
        btn.tag = i;
        [btn setImage:[BaseImge zlc_imageNamed:iconArr[i] ofType:@".png"] forState:UIControlStateNormal];
        btn.frame = CGRectMake((76+i*78)*kScale, 65*kScale, 30*kScale, 30*kScale);
        [view addSubview:btn];
    }
    
    //按钮
    UIButton *changeBtn = [A2BaseTool title:@"立即补充" titleColor:WColor andFont:16 target:self action:@selector(addWater)];
    changeBtn.frame = CGRectMake(55*kScale, 115*kScale, 150*kScale, 50*kScale);
    [changeBtn.layer setCornerRadius:3*kScale];
    [changeBtn.layer setMasksToBounds:YES];
    changeBtn.backgroundColor = RGBColor(248, 164, 0);
    [view addSubview:changeBtn];
    
}
//补水
- (void)addWater{
    //修改数据
    NSMutableDictionary *replaceDic = [NSMutableDictionary dictionaryWithDictionary:_dic];
    
    if ([_typeStr isEqual: @"加水"]) {
        replaceDic[@"water"] = _showCount.text;
    }else{
        replaceDic[@"towel"] = _showCount.text;
    }
    
    [A2BaseTool showCompletionHUDWithMessage:@"补充成功" completion:nil];
    [A2CourtData sendChangeMsg:replaceDic andIndex:_index];
}

- (void)changeWaterCount :(UIButton *)btn{
    if (btn.tag == 0) {
        //减
        if (_count > 1) {
            _count--;
        }
    }else{
        //加
        _count++;
    }
    _showCount.text =  [NSString stringWithFormat:@"%ld",(long)_count];
}
@end
