//
//  A2BaseNav.h
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface A2BaseNav : UINavigationController

@end

NS_ASSUME_NONNULL_END
