//
//  SetMarqueeView.m
//  A2CourtManagement
//
//  Created by Nuo A on 2021/4/23.
//

#import "SetMarqueeView.h"
#import "A2_PublicHeader.h"

@interface SetMarqueeView ()

/**内容输入*/
@property (nonatomic,strong)UITextView *contenInput;
/**颜色选择*/
@property (nonatomic,strong)UILabel *colorLable;
/**颜色列表*/
@property (nonatomic,strong)UIView *colorView;


@end

@implementation SetMarqueeView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self creatUI];
    }
    return self;
}

- (void)creatUI{
    //主视图
    UIView *mainView = [[UIView alloc]initWithFrame:CGRectMake(15*kScale, 160*kScale, 345*kScale, 225*kScale)];
    mainView.backgroundColor = RGBColor(83, 83, 83);
    [mainView.layer setCornerRadius:3];
    [mainView.layer setMasksToBounds:YES];
    [self addSubview:mainView];
    
    
    //点击手势
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideenSelf)];
    [self addGestureRecognizer:tapView];
    
    
    //输入框
    UITextView *contenInput = [[UITextView alloc]initWithFrame:CGRectMake(15*kScale, 15*kScale, 315*kScale, 140*kScale)];
    contenInput.textColor = WColor;
    contenInput.font = fontSize(14);
    contenInput.backgroundColor = CColor;
    _contenInput = contenInput;
    [mainView addSubview:contenInput];
    
    //颜色选择
    UIButton *colorBtn = [A2BaseTool title:@"字体颜色" titleColor:WColor andFont:14 target:self action:@selector(colorBtnClick)];
    colorBtn.frame = CGRectMake(15*kScale, 164.5*kScale, 60*kScale, 46*kScale);
    colorBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    UILabel *textLable = [A2BaseTool textColor:RGBColor(153, 153, 153) fonSize:20 align:1];
    textLable.frame = CGRectMake(0, 0, 60*kScale, 20*kScale);
    textLable.text = @"A";
    [colorBtn addSubview:textLable];
    UILabel *colorLable = [[UILabel alloc]initWithFrame:CGRectMake(20*kScale, getY(textLable.frame)+2*kScale, 20*kScale, 3.7*kScale)];
    colorLable.backgroundColor = WColor;
    _colorLable = colorLable;
    [colorBtn addSubview:colorLable];
    [mainView addSubview:colorBtn];
    
    
    //滑动条
    UISlider *speedSlider = [[UISlider alloc]initWithFrame:CGRectMake(getX(colorBtn.frame)+14.5*kScale, 164.5*kScale, 80*kScale, 20*kScale)];
    [speedSlider setThumbImage:[BaseImge zlc_imageNamed:@"Slide button" ofType:@".png"]
                      forState:UIControlStateNormal];
    speedSlider.tintColor = RGBColor(153, 153, 153);
    [speedSlider addTarget:self action:@selector(changeScorSeepd :) forControlEvents:UIControlEventValueChanged];
    speedSlider.value = _scrollSpeed / 200;
    speedSlider.minimumValue = 0;
    speedSlider.maximumValue = 1;
    [mainView addSubview:speedSlider];
    
    //标题
    UILabel *seppdTitle = [A2BaseTool textColor:WColor fonSize:14 align:1];
    seppdTitle.frame = CGRectMake(getX(colorBtn.frame)+14.5*kScale, getY(speedSlider.frame)+6*kScale, 80*kScale, 20*kScale);
    seppdTitle.text = @"弹幕速度";
    [mainView addSubview:seppdTitle];
    
    
    
    //颜色列表
    UIView *colorView = [[UIView alloc]initWithFrame:CGRectMake(15*kScale, getY(mainView.frame)+30*kScale, 345*kScale, 60*kScale)];
    colorView.backgroundColor = [WColor colorWithAlphaComponent:0.15];
    colorView.hidden = YES;
    _colorView = colorView;
    [self addSubview:colorView];
    
    NSArray *colorArr = @[WColor,BColor,RGBColor(244, 147, 9),RGBColor(66, 161, 192),RGBColor(178, 25, 137),RGBColor(66, 212, 93)];
    for (int i=0; i<colorArr.count; i++) {
        UIButton *colorBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        colorBtn.frame = CGRectMake((10+i*56)*kScale, 10*kScale, 50*kScale, 50*kScale);
        colorBtn.backgroundColor = colorArr[i];
        [colorBtn.layer setCornerRadius:25*kScale];
        [colorBtn.layer setMasksToBounds:YES];
        [colorBtn addTarget:self action:@selector(selectTextColor :) forControlEvents:UIControlEventTouchUpInside];
        [colorView addSubview:colorBtn];
    }
    
    
    //保存按钮
    UIButton *saveBtn = [A2BaseTool title:@"保存" titleColor:WColor andFont:16 target:self action:@selector(saveBtnClick)];
    saveBtn.frame = CGRectMake(230*kScale, 170*kScale, 100*kScale, 40*kScale);
    [saveBtn.layer setCornerRadius:3];
    [saveBtn.layer setMasksToBounds:YES];
    saveBtn.backgroundColor = RGBColor(248, 164, 0);
    [mainView addSubview:saveBtn];
    
}

//保存
- (void)saveBtnClick{
    
    if (_contenInput.text.length < 1) {
        [A2BaseTool showTilieWithMessage:@"内容不能为空"];
    }else{
        self.saveSet(_colorLable.backgroundColor, _scrollSpeed, _contenInput.text);
    }
}

//改变播放速度
- (void)changeScorSeepd :(UISlider *)slider{
    _scrollSpeed = slider.value*200;
}
//选中颜色
- (void)selectTextColor :(UIButton *)btn{
    _colorLable.backgroundColor = btn.backgroundColor;
}

//切换颜色选择 显示.隐藏
- (void)colorBtnClick{
    _colorView.hidden = !_colorView.hidden;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}

- (void)hideenSelf{
    self.bubbleDissMiss();
}
@end
